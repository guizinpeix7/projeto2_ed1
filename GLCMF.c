#include <stdio.h>
#include <stdlib.h>
#define SIZE_OF_GLCM_MATRIX 256
#define SIZE_OF_IMAGE 1024

struct Imagens_e_funcoes{
    //[0],[1],[2],[3],[4],[5],[6],[7]
    // N , NE, E, SE , S, SW,  W,  NW
    int **Matriz_imagem;	    
    int **glcm[7]; 
    int **LOCALIZACAO_GLCM_ALOCADA[7],
        **LOCALIZACAO_GLCM_ALTERADA[7];   
}; 


int **aloca_matriz_glcm(int **glcm_LOC){
    glcm_LOC = (int**) malloc(SIZE_OF_GLCM_MATRIX*sizeof(int *)); 
    
    for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i++){
            *(glcm_LOC+i) = (int*) malloc(SIZE_OF_GLCM_MATRIX*sizeof(int)); 
    }
    return glcm_LOC;
}

int **GLCM(int **matriz_imagem,int** Matriz_glcm,int pixel_central,int pixel_vizinho){           
    int linha;
    int coluna;

    for(int i = 1 ; i < SIZE_OF_IMAGE-1; i++){
        for(int j = 1 ; j < SIZE_OF_IMAGE-1; j++){
            linha = matriz_imagem[i][j];
            coluna = matriz_imagem[i+(pixel_central)][j+(pixel_vizinho)]; 
            Matriz_glcm[linha][coluna]+=1;         
        }
    }
    return Matriz_glcm;
}

int GLCM_main(){
    int loc_x[8] = {-1,-1,0,+1,+1,+1,0,-1},
        loc_y[8] = {0,+1,+1,+1,0,-1,-1,-1}; 
 
    struct Imagens_e_funcoes imagens_e_funcoes[50]; 
    
    ////////////////////////////////////////THIS PART IS ONLY TO DEBUG//////////////////////////////////
    imagens_e_funcoes[0].Matriz_imagem = (int **) malloc(SIZE_OF_IMAGE*sizeof(int *));                  
    for(int i = 0 ; i< SIZE_OF_IMAGE; i++){                                                           //
	*(imagens_e_funcoes[0].Matriz_imagem+i) = (int*) malloc(SIZE_OF_IMAGE*sizeof(int));	
    }                                                                                                 //

    for(int i = 0 ; i < SIZE_OF_IMAGE ; i++){                                                         //
    	for(int j = 0 ; j < SIZE_OF_IMAGE ; j++){
		scanf("%d", &imagens_e_funcoes[0].Matriz_imagem[i][j]);                                       //
		getchar();
		//Por que armazeno a variavel no endereco ????????? e nao no ponteiro ? PREOLA                //
		//Talvez por que **A = A[size][size] != ***A 
	}	                                                                                              //
    }
   /////////////////////////////////////////////////////////////////////////////////////////////////////



    for(int i = 0 ; i < 8 ; i++){
        imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALOCADA[i] = aloca_matriz_glcm(imagens_e_funcoes[0].glcm[i]);
    }

    for(int i = 0 ; i < 8 ; i++){
        imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[i] = GLCM(imagens_e_funcoes[0].Matriz_imagem,imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALOCADA[i],loc_x[i],loc_y[i]);
    
    
    }
    
    //////////////////////////////////////THIS PART IS ONLY TO DEBUG PRINT/////////////////////////////////
    
    for(int matriz_glcm_POS = 0 ; matriz_glcm_POS < 8 ; matriz_glcm_POS++){
            for(int i = 0 ; i < SIZE_OF_GLCM_MATRIX ; i ++){                                            //
            printf("\n"); 
            for(int j = 0 ; j < SIZE_OF_GLCM_MATRIX ; j++){                                             //
                printf("%d ", imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[matriz_glcm_POS][i][j]);
            }                                                                                           //
        }   
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    
    
    puts("");
    
    return 0; 
}

int main(){
    GLCM_main();
    
    
    return 0; 
    
}



 /*for(int matriz = 0 ; matriz < 8 ; matriz++){
            for(int i = 0 ; i < 256 ; i ++){
            printf("\n"); 
            for(int j = 0 ; j < 256 ; j++){
                printf("%d ", imagens_e_funcoes[0].LOCALIZACAO_GLCM_ALTERADA[matriz][i][j]);
            }
        }   
    }
*/
